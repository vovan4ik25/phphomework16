<?php

    $search_words = array('php', 'html', 'интернет', 'Web');//массив слов

    $search_strings = array(
        'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
        'PHP - это распространенный язык программирования  с открытым исходным кодом.',
        'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
    );//массив строк

    echo '<h1>'.'Homework #16'.'</h1>';

    for ($i = 0; $i <= count($search_strings); $i++) {//вывод строк
        echo $search_strings[$i];
        echo '<br>';
        echo '<br>';
    }

    setlocale(LC_ALL, "ru_RU.UTF-8");//указание локали (подключение кирилици)

    foreach ($search_strings as $key=>$string) {//переборка строк
        $v = '';
        $k = 0;
        for ($i = 0; $i <= count($search_words); $i++) {
            if (preg_match('/' . $search_words[$i] . '/ui', $string)) {//регулярное выражение для поиска опредиленных слов в строках
                $k = $key+1;
                $v .= "$search_words[$i]".' ';
            }
        }
        echo 'В предложении №'.$k.' есть слова: '.$v;//вывод результата поиска
        echo '<br>';
    }
?>